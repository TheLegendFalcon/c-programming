#include <iostream>
using namespace std;

int main()
{
  float price, tip;

  cout << "Enter the price (pound): ";
  cin >> price;

  cout << "Enter the tip (percentage): ";
  cin >> tip;

  float tip_total = 1 + (tip/100);
  float total = price * (tip_total);

  cout << "The tip is " << tip << "%\n";
  cout << "The total is £" << total;
}

