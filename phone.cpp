#include <iostream>
using namespace std;

int main(){
  string phone;

  cout << "Enter the phone number: ";
  cin >> phone;

  string format = "(" + phone.substr(0,3) + ")" + " " + phone.substr(3,4) + " " + phone.substr(7,4);

  cout << "The formatted number is: " << format << "\n";
}
